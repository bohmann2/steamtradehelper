# -*- coding: utf-8 -*-
# Настройки программы по умолчанию.

# global
cancel_val = 0
login_val = "bohmannn"
passwd_val = "-"

# CСылка на продажу вещей
# https://steamcommunity.com/!LINK!/inventory?modal=1
link_id_val = "profiles/76561198028876338"

# buy
buy_percent_val = 0.73
buy_lower_percent_val = 1.08
buy_qty_val = 1
min_price_val = 50
max_price_val = 1000
min_qty_val = 40
start_page_val = 1
check_min_percent_val = 0.55
check_max_percent_val = 0.84
check_start_from_val = 1
check_end_from_val = 0

# sell
gap_percent_val = 0.04
place_lower_val = 0.10
sell_start_from_val = 1
sell_end_from_val = 0
resell_lot_position_val = 5
msg_box = 0

tags_csgo = list()
tags_artifact = list()
tags_dota = list()
tags_tf = list()
sell_games_ids = list()


def init():

    global tags_csgo
    tags_csgo.append(["tag_set_op10_characters", "Brok Fang AG", 0])
    tags_csgo.append(["tag_set_community_27", "Brok Fang", 0])
    tags_csgo.append(["tag_set_op10_ct", "Control", 0])
    tags_csgo.append(["tag_set_op10_t", "Havoc", 0])
    tags_csgo.append(["tag_set_op10_ancient", "Ancient", 0])
    tags_csgo.append(["tag_set_community_26", "Fracture", 0])
    tags_csgo.append(["tag_set_community_25", "Prism 2", 0])
    tags_csgo.append(["tag_set_canals", "Canals", 0])
    tags_csgo.append(["tag_set_op9_characters", "Div Net AG", 0])
    tags_csgo.append(["tag_set_community_23", "Div Net", 0])
    tags_csgo.append(["tag_set_norse", "North", 0])
    tags_csgo.append(["tag_set_stmarc", "StMarc", 0])
    tags_csgo.append(["tag_set_community_24", "CS20", 0])
    tags_csgo.append(["tag_set_xraymachine", "XRay", 0])
    tags_csgo.append(["tag_set_community_22", "Prism", 0])
    tags_csgo.append(["tag_set_blacksite", "Blacksite", 0])
    tags_csgo.append(["tag_set_community_21", "Rest Zone", 0])
    tags_csgo.append(["tag_set_inferno_2", "2018 Inferno", 0])
    tags_csgo.append(["tag_set_nuke_2", "2018 Nuke", 0])
    tags_csgo.append(["tag_set_community_20", "Horizon", 0])
    tags_csgo.append(["tag_set_community_19", "Clutch", 0])
    tags_csgo.append(["tag_set_community_18", "Spectrum2", 0])
    tags_csgo.append(["tag_set_community_17", "Op Hydra", 0])
    tags_csgo.append(["tag_set_community_16", "Spectrum", 0])
    tags_csgo.append(["tag_set_community_15", "Glove", 0])
    tags_csgo.append(["tag_set_gamma_2", "Gamma2", 0])
    tags_csgo.append(["tag_set_community_13", "Gamma", 0])
    tags_csgo.append(["tag_set_community_12", "Chroma3", 0])
    tags_csgo.append(["tag_set_community_11", "Wildfire", 0])
    tags_csgo.append(["tag_set_community_10", "Revolver", 0])
    tags_csgo.append(["tag_set_community_9", "Shadow", 0])
    tags_csgo.append(["tag_set_community_8", "Falchion", 0])
    tags_csgo.append(["tag_set_gods_and_monsters", "G and M", 0])
    tags_csgo.append(["tag_set_chopshop", "Chop Shop", 0])
    tags_csgo.append(["tag_set_kimono", "Rising Sun", 0])
    tags_csgo.append(["tag_set_community_7", "Chroma2", 0])
    tags_csgo.append(["tag_set_community_6", "Chroma", 0])
    tags_csgo.append(["tag_set_community_5", "Vanguard", 0])
    tags_csgo.append(["tag_set_cache", "Cache", 0])
    tags_csgo.append(["tag_set_overpass", "Overpass", 0])
    tags_csgo.append(["tag_set_cobblestone", "CobbleSt", 0])
    tags_csgo.append(["tag_set_baggage", "Baggage", 0])
    tags_csgo.append(["tag_set_community_4", "Breakout", 0])
    tags_csgo.append(["tag_set_community_3", "Huntsman", 0])
    tags_csgo.append(["tag_set_bank", "Bank", 0])
    tags_csgo.append(["tag_set_community_2", "Phoenix", 0])
    tags_csgo.append(["tag_set_esports_ii", "esp2013W", 0])
    tags_csgo.append(["tag_set_weapons_iii", "Arms Deal3", 0])
    tags_csgo.append(["tag_set_community_1", "Wint Off", 0])
    tags_csgo.append(["tag_set_train", "Train", 0])
    tags_csgo.append(["tag_set_dust_2", "Dust2", 0])
    tags_csgo.append(["tag_set_mirage", "Mirage", 0])
    tags_csgo.append(["tag_set_lake", "Lake", 0])
    tags_csgo.append(["tag_set_italy", "Italy", 0])
    tags_csgo.append(["tag_set_safehouse", "Safehouse", 0])
    tags_csgo.append(["tag_set_bravo_i", "Bravo", 0])
    tags_csgo.append(["tag_set_weapons_ii", "Arms Deal2", 0])
    tags_csgo.append(["tag_set_bravo_ii", "Alpha", 0])
    tags_csgo.append(["tag_set_aztec", "Aztec", 0])
    tags_csgo.append(["tag_set_dust", "Dust", 0])
    tags_csgo.append(["tag_set_vertigo", "Vertigo", 0])
    tags_csgo.append(["tag_set_militia", "Militia", 0])
    tags_csgo.append(["tag_set_inferno", "Inferno", 0])
    tags_csgo.append(["tag_set_nuke", "Nuke", 0])
    tags_csgo.append(["tag_set_office", "Office", 0])
    tags_csgo.append(["tag_set_esports", "eSports", 0])
    tags_csgo.append(["tag_set_assault", "Assault", 0])
    tags_csgo.append(["tag_set_weapons_i", "Arms Deal", 0])

    global tags_artifact
    tags_artifact.append(["", "All items", 0])
    
    global tags_dota
    tags_dota.append(["", "All items", 0])
    
    global tags_tf
    tags_tf.append(["", "All items", 0])

    global sell_games_ids
    sell_games_ids.append(["730", "CSGO", 1])
    sell_games_ids.append(["583950", "Artifact", 0])
    sell_games_ids.append(["570", "Dota", 0])
    sell_games_ids.append(["440", "Team Fort", 0])
